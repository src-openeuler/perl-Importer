Name:           perl-Importer
Version:        0.026
Release:        1
Summary:        Alternative interface to modules that export symbols
License:        GPL+ or Artistic
URL:            https://metacpan.org/release/Importer
Source0:        https://cpan.metacpan.org/authors/id/E/EX/EXODIST/Importer-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  make
BuildRequires:  perl-generators
BuildRequires:  perl-interpreter
 
%description
This Perl module acts as a layer between Exporter and modules which consume
exports. It is feature-compatible with Exporter, plus some much needed
extras. You can use this to import symbols from any exporter that follows
Exporters specification. The exporter modules themselves do not need to use
or inherit from the Exporter module, they just need to set @EXPORT and/or
other variables.
 
%prep
%setup -q -n Importer-%{version}
 
%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
make %{?_smp_mflags}
 
%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*
 
%check
make test
 
%files
%license LICENSE
%doc Changes README
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 0.026-1
- Upgrade to version 0.026

* Mon Aug 3 2020 dingyue <dingyue5@huawei.com> -0.025 -1
- Package init
